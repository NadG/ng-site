import React from 'react';
import nb from 'components/molecules/NavBar/NavBar.module.scss';
import LabelNavLink from 'components/atoms/LabelNavLink/LabelNavLink';

const NavBar: React.FC = ({ children, ...props }) => {
  return (
    <div className={nb.container}>
      <LabelNavLink routeName="about" titleRoute="About" />
      <LabelNavLink routeName="projects" titleRoute="projects" />
      <LabelNavLink routeName="side-projects" titleRoute="side projects" />
      <LabelNavLink routeName="contacts" titleRoute="contacts" />
    </div>
  );
};

export default NavBar;
