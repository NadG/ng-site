import React from 'react';
import sl from 'components/molecules/SocialLogo/SocialLogo.module.scss';
import { ReactComponent as Gitlab } from 'styles/icons/gitlab.svg';
import { ReactComponent as Codepen } from 'styles/icons/codepen.svg';
import { ReactComponent as In } from 'styles/icons/in.svg';
import { ReactComponent as Dev } from 'styles/icons/devto.svg';
import { ReactComponent as Insta } from 'styles/icons/insta.svg';
import { ReactComponent as Email } from 'styles/icons/email.svg';
import cn from 'classnames';

interface Props {
  link: string;
  social: string;
  //social: 'devto' | 'linkedin' | 'codepen' | 'gitlab' | 'instagram';
}
const SocialLogo: React.FC<Props> = ({ children, ...props }) => {
  return (
    <a href={props.link} target="_blank" rel="noopener noreferrer">
      {props.social === 'mail' && (
        <Email className={cn(sl.logo, sl.logo_email)} />
      )}
      {props.social === 'devto' && <Dev className={sl.logo} />}
      {props.social === 'linkedin' && <In className={sl.logo} />}
      {props.social === 'codepen' && <Codepen className={sl.logo} />}
      {props.social === 'gitlab' && <Gitlab className={sl.logo} />}
      {props.social === 'instagram' && <Insta className={sl.logo} />}
    </a>
  );
};

export default SocialLogo;
