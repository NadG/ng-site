import React from 'react';
import abl from './AboutBulletList.module.scss';
import ListItem from 'components/molecules/ListItem/ListItem';

const AboutBulletList: React.FC = () => {
  return (
    <div className={abl.listContainer}>
      <ListItem>
        <span className={abl.title}>
          I have <span className={abl.bold}> 5 years</span>  of experience in web app development, both for b2b and b2c.
        </span>
      </ListItem>
    
    
    </div>
  );
};

export default AboutBulletList;
