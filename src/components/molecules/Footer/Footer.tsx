import React from 'react';
import f from 'components/molecules/Footer/Footer.module.scss';
import Title from 'components/atoms/Title/Title';
import Sticker from 'components/atoms/Sticker/Sticker';
import Flex from 'components/atoms/Flex/Flex';
import { ReactComponent as Gitlab } from 'styles/icons/gitlab.svg';

const Footer: React.FC = ({ children }) => {
  return (
    <div className={f.footer_container}>
      <Flex row fullWidth jcCenter>
        <Sticker>
          <Flex column jcCenter aiCenter>
            <Title paragraph>
              Website coded in React, Typescript and Sass.
            </Title>
            <a
              href="https://gitlab.com/NadG/ng-site"
              target="_blank"
              rel="noopener noreferrer"
            >
              <Flex row aiCenter>
                <Title paragraph>Here's the code!</Title>
                <Gitlab className={f.logo_footer} />
              </Flex>
            </a>
          </Flex>
        </Sticker>
      </Flex>
    </div>
  );
};

export default Footer;
