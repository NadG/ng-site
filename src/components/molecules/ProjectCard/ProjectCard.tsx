import Flex from 'components/atoms/Flex/Flex';
import pc from 'components/molecules/ProjectCard/ProjectCard.module.scss';
import React from 'react';
import { ReactComponent as Angular } from 'styles/icons/angular.svg';
import { ReactComponent as Ionic } from 'styles/icons/ionic.svg';
import { ReactComponent as Mobx } from 'styles/icons/mobx.svg';
import { ReactComponent as Php } from 'styles/icons/php.svg';
import { ReactComponent as Reactx } from 'styles/icons/react.svg';
import { ReactComponent as Ts } from 'styles/icons/ts.svg';
import Box from 'components/atoms/Box/Box';


interface Props {
  title?: string;
  technologies?: string[];
  company?: string;
  description?: string;
  link: string;
}

const Footer: React.FC<Props> = ({ children, ...props }) => {
  return (
    <Box>
      <a className={pc.container} href={props.link} target="_blank" rel="noopener noreferrer" >
        <Flex column jcCenter aiCenter fullWidth>
          <h3 className={pc.title}>{props.title}</h3>
          <Flex row aiCenter>
            <span className={pc.techLogo}>
              {props.technologies?.map((tech, i) => {
                switch (tech) {
                  case 'react':
                    return <Reactx />
                  case 'angular':
                    return <Angular />
                  case 'ionic':
                    return <Ionic />
                  case 'mobx':
                    return <Mobx />
                  case 'php':
                    return <Php />
                  case 'typescript':
                    return <Ts />
                }
              }
              )}
            </span>
          </Flex>
          <Flex row aiCenter>
            {props.technologies?.map((tech, i) =>
              i < props.technologies!.length - 1 ? <h4 className={pc.techDescription}> {tech} <span className={pc.goldDot}>• </span></h4> : <h4 className={pc.techDescription}> {tech}</h4>
            )}
          </Flex>
          <p className={pc.description}>{props.description}</p>
          {props.company && (
            <Flex row aiCenter>
              <h3 className={pc.company}>at {props.company}</h3>
            </Flex>
          )
          }
        </Flex>
      </a>
    </Box>
  );
};

export default Footer;
