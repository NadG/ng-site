import React from 'react';
import Title from 'components/atoms/Title/Title';
import Flex from 'components/atoms/Flex/Flex';

interface Props {
  text?: string;
}
const ListItem: React.FC<Props> = ({ children, ...props }) => {
  return (
    <Flex row aiCenter>
      {props.text ? (
        <Title title={props.text} paragraph />
      ) : (
        <Title>{children}</Title>
      )}
    </Flex>
  );
};

export default ListItem;
