import React from 'react';
import b from 'components/atoms/Box/Box.module.scss';
import cn from 'classnames';

interface Props {
  type?: 'gold' | 'black' | 'white';
}
const Box: React.FC<Props> = ({ children, ...props }) => {
  return (
    <div className={cn(b.container, {
      [b.black]: props.type === 'black',
      [b.gold]: props.type === 'gold',
      [b.white]: props.type === 'white',
    })}>
      {children}
    </div>
  );
};

export default Box;
