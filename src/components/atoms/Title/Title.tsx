import React from 'react';
import t from 'components/atoms/Title/Title.module.scss';
import cn from 'classnames';

interface Props {
  title?: string;
  heading?: boolean;
  subHeading?: boolean;
  sectionTitle?: boolean;
  subtitle?: boolean;
  paragraph?: boolean;
}
const Title: React.FC<Props> = ({ children, ...props }) => {
  return (
    <p
      className={cn({
        [t.heading]: props.heading,
        [t.subHeading]: props.subHeading,
        [t.subtitle]: props.subtitle,
        [t.sectionTitle]: props.sectionTitle,
        [t.paragraph]: props.paragraph,
      })}
    >
      {props.title ? props.title : children}
    </p>
  );
};

export default Title;
