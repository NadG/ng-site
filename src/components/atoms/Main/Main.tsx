import React, { useEffect, useState } from 'react';
import main from './Main.module.scss';
import About from 'components/pages/About/About';
import Projects from 'components/pages/Projects/Projects';
import SideProjects from 'components/pages/SideProjects/SideProjects';
import Contacts from 'components/pages/Contacts/Contacts';
import {
  Switch,
  Route,
  useLocation,
  Redirect,
  withRouter,
} from 'react-router-dom';
import NavBar from 'components/molecules/NavBar/NavBar';
import Binder from 'components/atoms/Binder/Binder';
import Title from 'components/atoms/Title/Title';
import cn from 'classnames';
import useWindowSize from 'hooks/useWindowSize';

const Main = () => {
  const location = useLocation();
  const [mobileMenuVisible, setMobileMenuVisible] = React.useState(false);
  const [mobileMenu, setMobileMenu] = useState(false);
  const [width, height] = useWindowSize();

  useEffect(() => {
    if (width < 1200) {
      setMobileMenuVisible(true);
      setMobileMenu(true);
    } else {
      setMobileMenu(false);
    }
  }, [width, height, mobileMenuVisible, mobileMenu]);

  return (
    <div className={main.book_container}>
      <div id="title" className={main.title_container}>
        {location.pathname === '/about' && (
          <div>
            <Title title="Hi, I'm Nadia Guarracino," heading />
            <Title title="Front-End developer from Milan" subHeading />
          </div>
        )}
        {location.pathname === '/blog' && (
          <Title title="Coming soon." heading />
        )}
        {location.pathname === '/contacts' && (
          <div>
            <Title title="Contact me." heading />
            <Title title="Let's get in touch" subHeading />
          </div>
        )}
        {location.pathname === '/projects' && (
          <Title title="Main career projects." heading />
        )}
        {location.pathname === '/skills' && <Title title="Skills." heading />}
        {location.pathname === '/side-projects' && (
          <Title title="Side projects." heading />
        )}
      </div>
      <div id="rings" className={main.rings_container}>
        <Binder />
      </div>
      <div id="content" className={main.content_container}>
        <div id="content_inner" className={main.content_inner}>
          <Switch>
            <Route exact path="/">
              <Redirect to="/about" />
            </Route>
            <Route exact path="/about" component={About} />
            <Route exact path="/projects" component={Projects} />
            <Route exact path="/side-projects" component={SideProjects} />
            <Route exact path="/contacts" component={Contacts} />
          </Switch>
          <Route path="*" exact={true}>
            <Redirect to="/about" />
          </Route>
        </div>
      </div>
      <div
        id="navlinks"
        className={cn(main.navlinks_container, main.fade, {
          [main.fade_in]: mobileMenuVisible && mobileMenu,
          [main.fade_out]: !mobileMenuVisible && mobileMenu,
        })}
      >
        <NavBar />
      </div>
    </div>
  );
};
export default withRouter(Main);
