import React from 'react';
import b from 'components/atoms/Binder/Binder.module.scss';
import { ReactComponent as Ring } from 'styles/icons/binder.svg';

const Binder: React.FC = () => {
  return (
    <div className={b.wrapper}>
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
      <Ring />
    </div>
  );
};

export default Binder;
