import React from 'react';
import ib from 'components/atoms/IconBox/IconBox.module.scss';
import Flex from 'components/atoms/Flex/Flex';

interface Props {
  title: string;
  link?: string;
}
const IconBox: React.FC<Props> = ({ title, link, children }) => {
  return (
    <Flex aiCenter row>
      {children}
      {link ?
        <a
          className={ib.titleLink}
          href={link}
          target="_blank"
          rel="noopener noreferrer"
        >
          {title}
        </a>
        : <p className={ib.title}>{title}</p>}
    </Flex>
  );
};

export default IconBox;
