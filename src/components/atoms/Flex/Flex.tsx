import React from 'react';
import cn from 'classnames';
import flex from './Flex.module.scss';

interface FlexProps {
  fullHeight?: boolean;
  fullWidth?: boolean;
  column?: boolean;
  row?: boolean;
  wrap?: boolean;
  rowReverse?: boolean;
  jcBetween?: boolean;
  jcAround?: boolean;
  jcCenter?: boolean;
  aiCenter?: boolean;
}

const Flex: React.FC<FlexProps> = ({ ...props }) => {
  return (
    <div
      style={{
        height: props.fullHeight ? '100%' : 'auto',
        width: props.fullWidth ? '100%' : 'auto',
      }}
      className={cn({
        [flex.row]: props.row,
        [flex.column]: props.column,

        [flex.row_reverse]: props.rowReverse,

        [flex.jc_sb]: props.jcBetween,
        [flex.jc_a]: props.jcAround,
        [flex.jc_c]: props.jcCenter,

        [flex.ai_c]: props.aiCenter,

        [flex.wrap]: props.wrap,
      })}
    >
      {props.children}
    </div>
  );
};

export default Flex;
