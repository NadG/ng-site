import React, { useEffect, useState } from 'react';
import nb from 'components/atoms/LabelNavLink/LabelNavLink.module.scss';
import { NavLink } from 'react-router-dom';
import cn from 'classnames';
import { ReactComponent as Contacts } from 'styles/icons/contacts.svg';
import { ReactComponent as Projects } from 'styles/icons/job.svg';
import { ReactComponent as SideProjects } from 'styles/icons/side.svg';
import useWindowSize from 'hooks/useWindowSize';
import Title from 'components/atoms/Title/Title';

export type Routes =
  | 'contacts'
  | 'about'
  | 'skills'
  | 'projects'
  | 'side-projects';

interface Props {
  routeName: Routes;
  titleRoute: string;
}
const LabelNavLink: React.FC<Props> = ({ routeName, titleRoute }) => {
  const [width, height] = useWindowSize();
  const [mobileMenu, setMobileMenu] = useState(false);

  useEffect(() => {
    if (width < 1200) {
      return setMobileMenu(true);
      //return setMobileMenuHeight(height - 65);
    } else {
      return setMobileMenu(false);
      //return setMobileMenuHeight(0);
    }
  }, [width, height]);
  return (
    <NavLink
      to={`/${routeName}`}
      className={cn(nb.link, {
        [nb.accent]: routeName === 'contacts',
        [nb.normal]: routeName !== 'contacts',
        [nb.smallScreensAlignment]: mobileMenu,
      })}
      activeClassName={cn({
        [nb.active_normal]: routeName !== 'contacts',
        [nb.active_accent]: routeName === 'contacts',
      })}
    >
      <>
        {!mobileMenu ? titleRoute : null}
        {mobileMenu && routeName === 'about' && (
          <div className={nb.about}>
            <Title title="NG" subHeading />
          </div>
        )}
        {mobileMenu && routeName === 'contacts' && (
          <Contacts className={nb.navIcon} />
        )}
        {mobileMenu && routeName === 'projects' && (
          <Projects className={nb.navIcon} />
        )}
        {mobileMenu && routeName === 'side-projects' && (
          <SideProjects className={nb.navIcon} />
        )}
      </>
    </NavLink>
  );
};

export default LabelNavLink;
