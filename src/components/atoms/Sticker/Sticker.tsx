import React from 'react';
import s from 'components/atoms/Sticker/Sticker.module.scss';

const Sticker: React.FC = ({ children }) => {
  return <div className={s.container}>{children}</div>;
};

export default Sticker;
