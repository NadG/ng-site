import React from 'react';
import IconBox from 'components/atoms/IconBox/IconBox';
import { ReactComponent as Game } from 'styles/icons/game.svg';
import { ReactComponent as Cook } from 'styles/icons/cook.svg';
import { ReactComponent as Reactt } from 'styles/icons/react.svg';
import { ReactComponent as FE } from 'styles/icons/fe.svg';
import { ReactComponent as Personality } from 'styles/icons/perso.svg';
import Flex from 'components/atoms/Flex/Flex';

const AboutIconBox: React.FC = () => 
   (
      <Flex column> 
        <IconBox title="React enthusiast">
          <Reactt />
        </IconBox>
        <IconBox title="Front-End lover">
          <FE />
        </IconBox>
        <IconBox title="INTP-T Personality" link="https://www.16personalities.com/profiles/f7c99d3e88c79">
          <Personality />
        </IconBox>
        <IconBox title="Passionate videogamer" link="https://steamcommunity.com/profiles/76561198089567749">
          <Game />
        </IconBox>
        <IconBox title="Creative cooker">
          <Cook />
        </IconBox>
      </Flex>
  );

export default AboutIconBox;
