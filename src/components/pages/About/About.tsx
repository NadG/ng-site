import React, { useEffect } from 'react';
import about from 'components/pages/About/About.module.scss';
import Title from 'components/atoms/Title/Title';
import me from 'styles/images/me.jpg';
import Sticker from 'components/atoms/Sticker/Sticker';
import { dictionary } from 'dictionary/about';
import { withRouter } from 'react-router-dom';
import AboutIconBox from 'components/organisms/AboutIconBox/AboutIconBox';
import Flex from 'components/atoms/Flex/Flex';
//import meLarge from 'styles/images/me-large.jpg';
//import meMedium from 'styles/images/me-medium.jpg';
//import meSmall from 'styles/images/me-small.jpg';

const About: React.FC = () => {
  //const [width] = useWindowSize();

  useEffect(() => {
    console.log('about');
    window.pageYOffset !== 0 && window.scrollTo(0, 0);
  }, []);

  return (
    <Flex column>
      <div className={about.container}>
        <Sticker>
          <Title title={dictionary.pageTitle} subHeading />
        </Sticker>
        <div className={about.contentContainer}>
          <div className={about.imageContainer}>
            <img src={me} alt="Nadia Guarracino" className={about.me} />
          </div>
          <div className={about.iconBoxContainer}>
            <AboutIconBox />
          </div>
        </div>
      </div>
      <br />
      <div className={about.descriptionContainer}>
        <p >Currently I am employed ad Dottori.it.</p>
        <p>My front-end stack evolved during these 5 years of experience, ending up to have worked with <span className={about.goldLabel}>Angular</span> (since the 1.3 version), <span className={about.goldLabel}>React</span> and <span className={about.goldLabel}>Vue</span>." </p>
        <p>

        </p>
        <p>
          When I'm not working you can find me working on my side projects (currently I am learning <span className={about.goldLabel}>Flutter</span>), play videogames, cooking or play with my veeery old electric guitar.
        </p>
      </div>
    </Flex>
  );
};

export default withRouter(About);
/*
{width < 768 && (
            <img src={me} alt="Nadia Guarracino" className={about.me} />
          )}
          {width >= 768 && width < 1200 && (
            <img src={me} alt="Nadia Guarracino" className={about.me} />
          )}
          {width >= 1200 && width < 2560 && (
            <img src={me} alt="Nadia Guarracino" className={about.me} />
          )}
          {width >= 2560 && (
          )}
 */