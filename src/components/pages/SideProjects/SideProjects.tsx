import React, { useEffect } from 'react';
import sp from 'components/pages/SideProjects/SideProjects.module.scss';
import Title from 'components/atoms/Title/Title';
import Sticker from 'components/atoms/Sticker/Sticker';
import { dictionary } from 'dictionary/sideProjects';
import { withRouter } from 'react-router-dom';
import ProjectCard from 'components/molecules/ProjectCard/ProjectCard';

const SideProjects: React.FC = () => {
  useEffect(() => {
    console.log('sp');
    (window.pageYOffset !== 0 || document.documentElement.scrollTop !== 0) &&
      window.scrollTo(0, 0);
  }, []);

  return (
    <div className={sp.content}>
      {dictionary.apps.map((app, i) => (
        <ProjectCard
          key={i}
          title={app.title}
          technologies={app.technologies}
          description={app.description}
          link={app.link}
        />
      ))}
      <br/>
      <span>
        <Sticker>
        <Title title="Currently I am learning Flutter, so stay tuned for my new upcoming projects :)" paragraph />
      </Sticker>
      </span>

    </div>
  );
};

export default withRouter(SideProjects);
