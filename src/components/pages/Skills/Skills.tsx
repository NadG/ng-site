import React, { useEffect } from 'react';
import Title from 'components/atoms/Title/Title';
import { HorizontalBar } from 'react-chartjs-2';
import Sticker from 'components/atoms/Sticker/Sticker';
import { dictionary } from 'dictionary/skills';
import { withRouter } from 'react-router-dom';
import Flex from 'components/atoms/Flex/Flex';

const Skills: React.FC = () => {
  useEffect(() => {
    window.pageYOffset !== 0 && window.scrollTo(0, 0);
  }, []);

  return (
    <Flex column>
      <Sticker>
        <Title title={dictionary.pageTitle} subHeading />
      </Sticker>
      <br />
      <Title title={dictionary.paragraph1} paragraph />
      <br />
      <HorizontalBar
        data={dictionary.graphData}
        options={{
          responsive: true,
          maintainAspectRatio: true,
        }}
      />
      <br />
      <Title title={dictionary.paragraph2} paragraph />
    </Flex>
  );
};

export default withRouter(Skills);
