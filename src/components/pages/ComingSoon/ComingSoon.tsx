import React from 'react';
import b from 'components/pages/ComingSoon/ComingSoon.module.scss';
import Title from 'components/atoms/Title/Title';

const ComingSoon: React.FC = () => {
  return (
    <div className={b.content}>
      <Title title="Blog" heading />
      (I promise)
    </div>
  );
};

export default ComingSoon;
