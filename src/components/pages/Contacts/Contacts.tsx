import React, { useEffect } from 'react';
import c from 'components/pages/Contacts/Contacts.module.scss';
import Title from 'components/atoms/Title/Title';
import Flex from 'components/atoms/Flex/Flex';
import cn from 'classnames';
import Sticker from 'components/atoms/Sticker/Sticker';
import Footer from 'components/molecules/Footer/Footer';
import { dictionary } from 'dictionary/contacts';
import SocialLogo from 'components/molecules/SocialLogo/SocialLogo';
import { withRouter } from 'react-router-dom';

const Contacts: React.FC = () => {
  useEffect(() => {
    console.log('contacts');

    window.pageYOffset !== 0 && window.scrollTo(0, 0);
  }, []);

  return (
    <div className={cn(c.content, c.content_contacts)}>
      <Flex column jcBetween fullHeight>
        <Flex column jcCenter fullHeight>
          <Sticker>
            <Title title="Find me on the internet" subtitle />
          </Sticker>
<br/>
          <Flex row jcBetween fullWidth>
            {dictionary.socials.map((s, i) => (
              <SocialLogo key={i} link={s.link} social={s.social} />
            ))}
          </Flex>
        </Flex>
        <Footer />
      </Flex>
    </div>
  );
};

export default withRouter(Contacts);
