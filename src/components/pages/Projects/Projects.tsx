import React, { useEffect } from 'react';
import p from 'components/pages/Projects/Projects.module.scss';
import ProjectCard from 'components/molecules/ProjectCard/ProjectCard';
import { withRouter } from 'react-router-dom';

const Projects: React.FC = () => {
  useEffect(() => {
    console.log('projects');

    (window.pageYOffset !== 0 || document.documentElement.scrollTop !== 0) &&
      window.scrollTo(0, 0);
  }, []);

  return (
    <div className={p.content}>
      <div className={p.cardsContainer}>
        <ProjectCard
          title="dottori.it"
          technologies={['react', 'php', 'mobx', 'typescript']}
          description={'Online platform that meets doctors and their patients needs.'}
          link={'https://www.dottori.it'}
          company={'Dottori.it'}
        />
        <ProjectCard
          title="Salutile Vaccinazioni"
          technologies={['angular', 'ionic']}
          description={'Helps the citizen to view own electronic health record about the vaccinations done, the mandatory ones and those that are recommendend.'}
          link={'https://www.fascicolosanitario.regione.lombardia.it/app'}
          company={'Reply'}
        />

        <ProjectCard
          title="Salutile NoGlutine"
          technologies={['angular', 'ionic']}
          description={'Meant for celiac citizens to manage own the celiac disease budget.'}
          link={'https://www.fascicolosanitario.regione.lombardia.it/app'}
          company={'Reply'}
        />

        <ProjectCard
          title="Zampa zampa"
          technologies={['angular', 'ionic']}
          description={'Access the Animal Registry of affection to find dogs and cats that can be adopted free of charge in Lombardy'}
          link={'https://www.fascicolosanitario.regione.lombardia.it/app'}
          company={'Reply'}
        />

        <ProjectCard
          title="Edison Energia"
          technologies={['angular', 'ionic']}
          description={'Manage the gas and electricity supplies of the clients'}
          link={'https://edisonenergia.it/edison/scarica-app'}
          company={'Reply'}
        />
      </div>
    </div>
  );
};

export default withRouter(Projects);
