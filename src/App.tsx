import React from 'react';
import app from 'App.module.scss';
import './styles/normalize.css';
import Main from './components/atoms/Main/Main';
import { BrowserRouter } from 'react-router-dom';

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <div className={app.container}>
        <Main />
      </div>
    </BrowserRouter>
  );
};

export default App;
