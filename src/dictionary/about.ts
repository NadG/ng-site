export const dictionary = {
  pageTitle: 'About me',
  /*definitions: [
    {
      title: 'React enthusiast',
    },
    {
      title: 'Front-End lover',
    },
    {
      title: 'INTP-T Personality',
      link: 'https://www.16personalities.com/profiles/f7c99d3e88c79',
    },
    {
      title: 'Passionate videogamer',
      link: 'https://steamcommunity.com/profiles/76561198089567749',
    },
    { title: 'Creative cooker' },
  ],*/
  listItem: [
    'I have 4+ years of experience on developing web-apps both for the public and internals.',
    'I have 3 years of experience in Angular* frameworks (from the 1.3 to 7), while in the last almost 2 years I am' +
    ' fully committed on ReactJS.',
    'Know how to use main CSS Frameworks like Bootstrap, Foundation and Bulma. Also, I have developed some projects' +
    ' following Material Design indications, altough I' +
    ' prefer to develop my' +
    ' own' +
    ' design and rules when given the possibility',
    'My main focus is creating beautiful, responsive and cross-browser web applications, as I love to pay attention on details and to provide the best UI/UX experience.'
  ],
  sections: [
    {
      title: 'Summary',
    },
    {
      title: 'My story in a nutshell',
      listItem: [''],
    },
  ],
};
