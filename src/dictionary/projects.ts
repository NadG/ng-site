export const dictionary = {
  pageTitle: 'Career relevant projects',
  dottoriItemList: [
    'The most used and most famous online platform that meets doctors and their patients needs.',
    'Developed with the team the Patients Dashboard and updated the Official Site',
    'Thanks to my work on the official site and the Patients Dashboard the company conversion rate have increased by' +
      ' 50% in a year.',
  ],
  replyItemList: {
    apps: [
      {
        title: 'Salutile Vaccinazioni',
        description: [
          'Helps the citizen to view own electronic health record about the vaccinations done, the' +
            ' mandatory ones and those that are recommendend.',
        ],
      },
      {
        title: 'Salutile NoGlutine',
        description: [
          'Meant for celiac citizens to manage own the celiac disease budget.',
          'Checks if a gluten-free diet product is supplied by the NHS, and find the accredited retailers closest to  the user.',
        ],
      },
      {
        title: 'Zampa zampa',
        description: [
          'Access the Animal Registry of affection to find dogs and cats that can be adopted free of' +
            ' charge in Lombardy',
          'Helps to find own lost pet',
        ],
      },
    ],
  },
  edison: {
    title: 'Edison Energia',
    description: [
      'Manage the gas and electricity supplies of the clients',
      'Self-reading of the supplies meters with cellphone camera shoot. Access and payments of the bills.',
    ],
  },
  otherProjects:
    "Also, I've developed b2b administrative dashboards in Angular.js (1.2, 1.3, 1.5) and in Angular 2+, using Bootstrap, Foundation and Bulma for the styling",
};
