export const dictionary = {
  pageTitle: 'What I know',
  paragraph1:
    'Getting better on what I already know and learning something new are what I love of being about the' +
    ' front-end.',
  paragraph2:
    'I have experience in Angular.js, Angular 2+ and Vue but since 2018 I am fully committed to React.js',
  graphData: {
    datasets: [
      {
        data: [90, 90, 90, 80, 80, 70, 70, 70, 40, 50, 0],
        backgroundColor: [
          '#68665F',
          '#807547',
          '#9C863A',
          '#BA952A',
          '#DBA318',
          '#FFAE03',
          '#FEBB21',
          '#FDC83F',
          '#FDD35D',
          '#FDDD7B',
        ],
        fill: false,
        borderDash: [5, 5],
        label: 'Skills',
      },
    ],
    labels: [
      'HTML',
      'CSS',
      'SCSS',
      'Javascript',
      'React',
      'Typescript',
      'Git',
      'MobX',
      'Redux',
      'Adobe Illustrator',
    ],
  },
};
