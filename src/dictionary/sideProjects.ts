export const dictionary = {
  pageTitle: 'Personal projects',
  pageDescription:
    'Improving and enhancing them on my spare time and when I want to learn something new',
  apps: [
    {
      title: 'Basilico',
      link: 'https://basilico.now.sh/',
      technologies: ['react', 'typescript'],
      description:
        'Minimal and useful responsive web-app that lets you know which fruits and vegetables are seasonal each month, providing dedicated delicious recipes.',
    },
    {
      title: 'Zweihander',
      technologies: ['react', 'typescript'],
      link: 'https://zweihander-character-randomizer.now.sh/',
      description: 'RPG random character generator for the game Zweihander.',
    },
    /* {
       title: 'Positivity Jar',
       link: '/',
       description: [
         'A cute web interface to express gratitude and liftup the mood in sad days',
         'It allows to create your own jar that can be opened anytime you desire.',
         'During the period of the quarantine I wanted to create something  that could cheer-up my distant friend.',
       ],
     },*/
  ],
};
