export const dictionary = {
  pageTitle: "Find me on the internet",
  socials: [
    {
      social: 'mail',
      link: 'mailto:nad.guar@gmail.com',
    },
    {
      social: 'devto',
      link: 'https://dev.to/jack_garrus',
    },
    {
      social: 'linkedin',
      link: 'https://www.linkedin.com/in/nadia-guarracino17',
    },
    {
      social: 'codepen',
      link: 'https://codepen.io/NadGu/',
    },
    {
      social: 'gitlab',
      link: 'https://gitlab.com/NadG',
    },
    {
      social: 'instagram',
      link: 'https://www.instagram.com/nadyng/',
    },
  ],
};
