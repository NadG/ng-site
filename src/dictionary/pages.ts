export const dictionary = {
  about: {
    routeName: 'About',
    title: "Hi, I'm Nadia Guarracino",
    subtitle: 'Front-End developer from Milan',
  },
  skills: {
    routeName: 'Skills',
    title: 'Skills',
    pageTitle: 'What I know',
  },
  projects: {
    routeName: 'Career & projects',
    title: 'Career & Projects.',
    pageTitle: 'Relevant projects',
  },
  sideProjects: {
    routeName: 'Side-projects',
    title: 'Side Projects.',
    pageTitle: 'Personal projects',
  },
  contacts: {
    routeName: 'Contacts',
    title: 'Contact me',
    pageTitle: "Let''s get in touch!",
    subPageTitle: 'Find me on the internet:',
  },
};
